terraform {
  required_version = ">= 1.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35"
    }
  }
}

# get the IP addresses of the assigned IPUM GWs
data "openstack_networking_port_v2" "ipum-gws" {
  for_each = toset(var.assigned_ipums)
  name = "${each.key}gw"
}

# get the IDs of all ports on the control network
data "openstack_networking_port_ids_v2" "node-exporter-ports" {
  network_id = var.ctrl_network_id
}

# find all the details of instances created in the previous step
data "openstack_networking_port_v2" "nodeexp" {
  for_each = toset(data.openstack_networking_port_ids_v2.node-exporter-ports.ids)
  port_id = each.key
}

locals {
  gwips = [ for gw in data.openstack_networking_port_v2.ipum-gws: gw.all_fixed_ips[0]]
  nodeexps = [ for node in data.openstack_networking_port_v2.nodeexp: node.dns_name]
}


# get the access IP of the vpod-ctrl instance
data "openstack_networking_port_v2" "vipu-ctrl-port" {
    name = "${var.vpod_name}-ctrl"
}

resource "local_file" "monitoring_inventory_file" {
    content     = templatefile("${path.module}/templates/inventory.tpl", {
        vpod_name = var.vpod_name
        dns_suffix = var.dns_suffix
        ctrl_hostname = "${var.vpod_name}-ctrl"
        nodeexp = compact(local.nodeexps)
    })
    filename = "${path.module}/monitoring.inv"
}

resource "local_file" "monitoring_group_vars" {
    content     = templatefile("${path.module}/templates/group_vars.tpl", {
        assigned_ipums = local.gwips
        vpod_name = var.vpod_name
    })
    filename = "${path.module}/group_vars/all.yml"
}

resource "null_resource" "vpod-monitoring" {
    provisioner "local-exec" {
        command = "ansible-playbook -i ${path.module}/monitoring.inv ${path.module}/ansible/site.yml"
    }
    depends_on = [
      local_file.monitoring_inventory_file
    ]
}