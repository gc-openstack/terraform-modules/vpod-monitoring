# vpod-monitoring
Terraform Module: vpod-monitoring
=================================

This Terraform module makes use of Ansible to deploy a Prometheus based monitoring stack into the vPOD.  

---
**NOTE**
There is a known circular dependency when building the list of node-exporter hosts if new instances are being created/destroyed.   This is to do with how that list is being built.    If you encounter this issue, comment out the vpod-monitoring module configuration, apply other Terraform changes, then re-enable and apply.

This will be fixed in a future release

---

Prerequisites
=============
Packages
--------
* Ansible
* Cloudalchemy Prometheus Ansible collections

Setup
-----
    python3 -m venv tools
    source tools/bin/activate
    pip install wheel
    pip install -r ansible/requirements.txt
    ansible-galaxy install -r ansible/requirements.yml

Usage
=====
This module should be used once per vPOD.  However, it is limited to support only a single Poplar host, so if more Poplar hosts are provisioned, they will have to be added to the NFS options on the Pure Flashblade by hand. 

    module "vpod-monitoring" {
        source                  = "git::ssh://git@gitlab.com/gc-openstack/terraform-modules/vpod-monitoring.git"
        vpod_name               = var.vpod_name
        assigned_ipums          = var.assigned_ipums
        vpod_floatingip         = var.vpod_floatingip
        ctrl_network_id         = module.networks.ctrl_network_id
        depends_on = [
            module.vpod-ctrl
        ]
    }

with example tfvars:

    vpod_name         = "vpod7"
    ctrl_vlan         = 1234
    rnic_vlan         = 2345
    storage_vlan      = 3456
    cidr_hint         = 100
    ipum_csv          = "rnic_rack8.csv"
    vpod_floatingip   = "1.2.3.4"
    poplaraz          = "rack8-AZ"
    assigned_ipums    = ["rack8-ipum1", "rack8-ipum2", "rack8-ipum3", "rack8-ipum4", "rack8-ipum5", "rack8-ipum6", "rack8-ipum7", "rack8-ipum8", "rack8-ipum9", "rack8-ipum10", "rack8-ipum11", "rack8-ipum12", "rack8-ipum13", "rack8-ipum14", "rack8-ipum15", "rack8-ipum16" ]
    flavor            = "r6525.full"

You will also need to set an admin password in ansible/group_vars/grafana/vars.yml


Design
======
Overview
--------
This module will use Ansible to deploy Prometheus to a vPOD, which can then be federated for central monitoring.  The components are installed as below:
* vpod-ctrl
    * Prometheus
    * Grafana
    * Node Exporter
* vpod-poplar
    * Node Exporter
* vpod-bastion
    * Node Exporter

Prometheus is configured to scrape the node exporter instance on each host in a vPOD, along with the exporter enpoints on both the V-IPU server and each IPUM gateway instance (10.2.XX.YY:2112).

The list of hosts to monitor is found by checking for the ports on ctrl_network_id and looking up the hostname associated with each.   The IPUM gateway instances are found from the 'assigned_ipums' list. 

Implementation
--------------

The module creates some local resources to be consumed by Ansible, using information looked up about hosts and IPUM:
* Inventory file
* group_vars file

Then once they have been created, a null_resource resource triggers an Ansible playbook run. 
