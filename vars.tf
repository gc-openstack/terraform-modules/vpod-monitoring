variable "vpod_floatingip" {
    type = string
}

variable "vpod_name" {
  type = string
}

variable "assigned_ipums" {
  type = list(string)
}

variable "ctrl_network_id" {
  type = string
}

variable "dns_suffix" {
    type = string
    default = ".openstack.example.net"
}