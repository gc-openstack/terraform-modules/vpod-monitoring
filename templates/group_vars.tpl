ipum_exporters:
%{ for addr in assigned_ipums ~}
- "${addr}"
%{ endfor ~}
prometheus_external_labels:
  environment: "${vpod_name}" 