[vipuctrl]
${ctrl_hostname}  

[vipuctrl:vars]
ansible_ssh_common_args = "-o ProxyCommand='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -W %h:%p -q ipuuser@${vpod_name}${dns_suffix}'"
ansible_user=ubuntu

[instances]
%{ for node in nodeexp ~}
${node}
%{ endfor ~}

[prometheus:children]
vipuctrl

[alertmanager:children]
vipuctrl

[grafana:children]
vipuctrl

[exporters:children]
instances

[exporters:vars]
ansible_ssh_common_args = "-o ProxyCommand='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -W %h:%p -q ipuuser@${vpod_name}${dns_suffix}'"
ansible_user=ubuntu